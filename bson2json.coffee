#!/usr/local/bin/coffee
# Usage:
#  $ chmod +x bson2json.coffee
#  $ ./bson2json.coffee ../"MDB dump 2015-07-27"/admin_log.bson | less
fs=require 'fs'
bs=require 'bson-stream'

fs.createReadStream process.argv[2]
.pipe new bs
.on 'data',(o)->
	console.log JSON.stringify o

#!/usr/local/bin/coffee

fs=require 'fs'
bs=require 'bson-stream'

schema={}
records=0
keys={}

fs.createReadStream process.argv[2]
.pipe new bs
.on 'data',(o)->
	ks=Object.keys(o).sort()
	for k in ks
		keys[k]=true
	s=JSON.stringify ks
	schema[s] or=0
	schema[s]++
	records++
.on 'error',(err)->throw err
.on 'end',(o)->
	console.log 'Keys (union):',Object.keys keys
	console.log 'Schema variations:',(Object.keys schema).length,schema
	console.log records,'records'
